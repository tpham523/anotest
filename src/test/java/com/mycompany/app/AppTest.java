package com.mycompany.app;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.Before;
import org.junit.Test;
import org.junit.After;
import static org.junit.Assert.*;

/**
 * Unit test for simple App.
 */
public class AppTest
{

      @Test
    public void testConcatenate() {
        App myUnit = new App();

        String result = myUnit.concatenate("one", "two");

        assertEquals("onetwo", result);

    }

}
